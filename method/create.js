const { isValidSubType, VALID_SUB_TYPES } = require('../spec/lib/is-valid-sub-type')

module.exports = function Create (_, findOrCreateCrut, type) {
  return function create (subType, blob, input, cb) {
    // check the subType passed in matches the allowed artefact types we currently support
    if (!isValidSubType(subType)) return cb(new Error(`create expects the subType to be: ${VALID_SUB_TYPES}`))

    const crut = findOrCreateCrut(`${type}/${subType}`)

    crut.create({ ...input, blob }, cb)
  }
}
