const CRUT = require('ssb-crut-authors')
const Artefact = require('../spec/artefact')

const ARTEFACT_TYPE = 'artefact'

module.exports = function Method (server) {
  const crutCache = {}

  function LoadCrut (spec) {
    return function loadCrut (id, cb) {
      server.get({ id, private: true, meta: true }, (err, root) => {
        if (err) return cb(err)

        cb(null, spec(root.value.content.type))
      })
    }
  }

  function Spec (typeSpec) {
    return function spec (subType) {
      if (!crutCache[subType]) crutCache[subType] = new CRUT(server, typeSpec(subType))

      return crutCache[subType]
    }
  }

  function buildAPI (typeSpec, subType) {
    const spec = Spec(typeSpec)
    return {
      create: require('./create')(server, spec, subType),
      update: require('./update')(server, LoadCrut(spec)),
      get: require('./get')(server, LoadCrut(spec), subType)
    }
  }

  return {
    ...buildAPI(Artefact, ARTEFACT_TYPE)
  }
}
