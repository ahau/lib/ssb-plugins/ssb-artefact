const edtf = require('edtf')

function IntervalChecker (field) {
  return function intervalChecker (content) {
    if (!content[field]) return true
    if (!content[field].set) return true

    try {
      const result = edtf.parse(content[field].set)
      if (result.type !== 'Interval') return new Error(`artefact expected ${field} to be an EDTF Interval, but got type ${result.type}`)
      return true
    } catch (e) {
      return new Error(`artefact expects ${field} to be set to an EDTF compatible interval, got ${content[field].set}`)
    }
  }
}

function DateChecker (field) {
  return function (content) {
    if (!content[field]) return true
    if (!content[field].set) return true

    try {
      const result = edtf.parse(content[field].set)
      if (result.type !== 'Date') return new Error(`artefact expected ${field} to be an EDTF Date, but got type ${result.type}`)
      return true
    } catch (e) {
      return new Error(`artefact expects ${field} to be set to an EDTF compatible date, got ${content[field].set}`)
    }
  }
}

module.exports = {
  IntervalChecker,
  DateChecker
}
