const Overwrite = require('@tangle/overwrite')

const overwrite = (schema) => Overwrite({ valueSchema: schema })

module.exports = {
  string: overwrite({ type: 'string' }),
  image: overwrite({ $ref: '#/definitions/image' }),
  integer: overwrite({ $ref: '#/definitions/integer' })
}
