const VALID_SUB_TYPES = [
  'photo',
  'video',
  'audio',
  'document'
]

function isValidSubType (subType) {
  return VALID_SUB_TYPES.includes(subType)
}

module.exports = {
  isValidSubType,
  VALID_SUB_TYPES
}
