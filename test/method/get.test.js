const test = require('tape')
const Server = require('../test-bot')
const blobId = '&1ZQM7TjQHBUEcdBijB6y7dkX047wCf4aXcjFplTjrJo=.sha256'
const unbox = 'YmNz1XfPw/xkjoN594ZfE/JUhpYiGyOOQwNDf6DN+54=.boxs'

const artefactBlob = (mimeType) => ({
  blobId,
  mimeType,
  size: 1212123,
  unbox
})

const pull = require('pull-stream')

test('artefact.get', t => {
  t.plan(6)

  const server = Server()

  function sharedDetails (type) {
    return {
      authors: { add: [server.id] },
      title: type + ' Hello',
      description: 'this is an artefact-' + type,
      createdAt: '2020-07-13',

      identifier: 'AF123456',
      language: 'English',
      licence: 'Attribution CC BY',
      rights: 'All rights belong to the Author Cherese Eriepa. Phone 0212345678',
      source: 'www.ahau.io',
      translation: 'Kia ora',
      location: 'Raglan'
      // recps: [server.id]
    }
  }

  const otherDetails = {
    duration: 11233,
    transcription: 'hello, to the world.'
  }

  const artefacts = [
    {
      type: 'audio',
      blob: artefactBlob('audio/mp3'),
      details: {
        ...sharedDetails('audio'),
        ...otherDetails
      }
    },
    {
      type: 'photo',
      blob: artefactBlob('image/png'),
      details: sharedDetails('photo')
    },
    {
      type: 'video',
      blob: artefactBlob('video/mp4'),
      details: {
        ...sharedDetails('video'),
        ...otherDetails
      }
    }
  ]

  pull(
    pull.values(artefacts),
    pull.asyncMap(({ type, blob, details }, cb) => {
      server.artefact.create(type, blob, details, cb)
    }),
    pull.collect((_, ids) => {
      ids.forEach((id, i) => {
        server.artefact.get(id, (_, data) => {
          const { type, details: artefact, blob } = artefacts[i]

          const fullType = 'artefact/' + type
          const expected1 = {
            key: id,
            type: fullType,
            originalAuthor: server.id,
            blob,
            recps: null,
            states: [],
            conflictFields: [],
            ...artefact,
            tombstone: null,
            authors: { [server.id]: [{ start: i, end: null }] }
          }

          if (type === 'audio' || type === 'video') {
            expected1.duration = artefact.duration
            expected1.transcription = artefact.transcription
          }

          t.deepEqual(data, expected1, 'gets reduced state of ' + type)

          const update = {
            title: 'NEW TITLE ' + type
          }

          if (i === 2) update.recps = [id] // test recps on the last one

          server.artefact.update(id, update, (err, res) => {
            if (i === 2 && err) {
              // see if the last artefacts update called back an error for the recps
              t.deepEqual(err, new Error('Cannot update recps field. Please check the details provided'), 'throws error on update recps ' + artefact.type)
              // this means the title wouldnt of been updated
              return
            }

            const expected2 = expected1

            if (res) {
              // if the artefact didnt callback an error it should have updated the title
              expected2.title = 'NEW TITLE ' + type
              expected2.key = res
            }

            server.artefact.get(id, (_, data) => {
              t.deepEqual(data.states[0], expected2.states[0], 'gets (updated) reduced state of ' + type)
              if (i === 1) server.close()
            })
          })
        })
      })
    })
  )
})
